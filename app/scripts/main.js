$(document).ready(function () {
    var analyticsPrefix = "SUPERMAXI/KIMBERLY";
    var _gaq = _gaq || [];
    _gaq.push(["_setAccount", "UA-10610797-1"]);
    _gaq.push(["_trackPageview"]);
    _gaq.push(["_trackEvent"]);
    _gaq.push(["_trackTiming"]);

    (function () {
        var ga = document.createElement("script");
        ga.type = "text/javascript";
        ga.async = true;
        ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(ga, s);
    })();

    // track de pagina
    function trackPage(strAccion)
    {
        _gaq.push(["_trackPageview", analyticsPrefix + strAccion]);
        setTrackTime(strAccion);
    }

    // track eventos
    function trackEvent(categoria, accion, etiqueta)
    {
        // tipo: _trackEvent, categories: Videos, actions: Play, labels: 
        _gaq.push(['_trackEvent', analyticsPrefix + categoria, analyticsPrefix + accion, analyticsPrefix + etiqueta]);
    }

    // track de tiempo
    var pageAnalytics = 'index';
    var startTimeAnalytics = new Date().getTime();
    var tiempoAnalytics = setTimeout(trackTime, 200);

    function setTrackTime(newPage)
    {
        clearTimeout(tiempoAnalytics);
        pageAnalytics = analyticsPrefix + newPage;
        startTimeAnalytics = new Date().getTime();
        tiempoAnalytics = setTimeout(trackTime, 200);
    }

    function trackTime(event)
    {
        var endTimeAnalytics = new Date().getTime();
        var timeSpentAnalytics = endTimeAnalytics - startTimeAnalytics;
        // _gaq.push([‘_trackTiming’, category, variable, time, opt_label, opt_sample]);
        _gaq.push(['_trackTiming', pageAnalytics, 'callback_timeout', timeSpentAnalytics]);
    }

    // track de medio
    jQuery.urlParam = function (name)
    {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);

        if (results == null)
        {
            return null;
        }
        else
        {
            return results[1] || 0;
        }
    }

    var trackMedio = jQuery.urlParam('medio');

    if (String(trackMedio) != '' && String(trackMedio) != 'undefined' && String(trackMedio) != 'null')
    {
        trackEvent('Ingreso ' + trackMedio, 'medio', trackMedio);
        trackPage(trackMedio);
    }
    //En cuanto cargue la pagine que registre la accion
    trackPage('carga-pagina');

    $(window).stellar();
    
    $('#restart_game').popover({
        content: 'Reiniciar juego',
        trigger: 'hover',
        delay: { show: 100, hide: 200 }
    });

    $('#restart_game').click(function () {
        restart();
    });

    $('span.prev').click(function () {
        $.scrollTo('#section1', 800);
    });

    $('#puzzle_mask').click(function () {
        $('#puzzle_mask').fadeOut(400);
        trackEvent('btn_empezar_juego', 'btn_empezar_juego', 'btn_empezar_juego');
        startGame();
    });

    $('#tryagain').click(function () {
        restart();
    });

    $('#contact-form').validate({// initialize the plugin
        rules: {
            nombre: {
                required: true,
                maxlength: 50
            },
            cedula: {
                required: true,
                maxlength: 13
            },
            telefono: {
                required: true,
                maxlength: 13,
                number: true
            },
            email: {
                required: true,
                maxlength: 30
            },
            ciudad: {
                required: true,
                maxlength: 30
            }
        }
    });

    //Enviar la información al servidor
    $('#send-info').click(function (e) {
        if ($('#contact-form').valid() && validarCedula($('#cedula').val(), '#display_errors')) {
            $.loader.open({
                title: 'Enviando información...',
                bgOpacity: 0.7,
                bgColor: '#000',
                fontColor: 'white',
                imgUrl: 'images/bloading.gif',
            });

            $('#myModalContactForm').modal('hide');
            trackEvent('btn_enviar_contacto', 'btn_enviar_contacto', 'btn_enviar_contacto');
            $.ajax({
                method: "POST",
                url: "../server/server.php",
                data: {
                    nombre: $('#nombre').val(),
                    cedula: $('#cedula').val(),
                    telefono: $('#telefono').val(),
                    email: $('#email').val(),
                    ciudad: $('#ciudad').val(),
                    minutos: tiempo.minuto,
                    segundos: tiempo.segundo
                }
            }).done(function (msg) {
                $('#successfull_register').modal('show');
                $.loader.close();
                setBoard();
            });
        }
    });

    var tiempoCorriendo = null;
    var tiempo = {
        segundo: 0,
        minuto: 0
    };
    
    function restart(){
        $('#myModalContactForm').modal('hide');
        trackEvent('btn_empezar_juego', 'btn_empezar_juego', 'btn_empezar_juego');
        setBoard();
        startGame();
    }

    function startGame() {
        $('#restart_game').css('display', 'inline-block');
        clearInterval(tiempoCorriendo);
        tiempo.minuto = 0;
        tiempo.segundo = 0;
        tiempoCorriendo = setInterval(function () {
            // Segundos
            tiempo.segundo++;
            if (tiempo.segundo >= 60) {
                tiempo.segundo = 0;
                tiempo.minuto++;
            }

            var textTime = (tiempo.minuto < 10 ? '0' + tiempo.minuto : tiempo.minuto) + ':' +
                    (tiempo.segundo < 10 ? '0' + tiempo.segundo : tiempo.segundo);
            $('#time_display').text(textTime);
        }, 1000);
    }

    function showContactForm() {
        $('#myModalContactForm').modal({
            backdrop: 'static'
        });

        //Muestra el tiempo final de resolucion del puzzle
        $('#puzzle-result').html(
                'Lo lograste en: ' + tiempo.minuto
                + ' minuto' + (tiempo.minuto > 1 ? 's ' : ' ') +
                tiempo.segundo + ' segundo' + (tiempo.segundo > 1 ? 's' : '')
                );
    }

    /**
     * Puzzle
     */
    var context = document.getElementById('puzzle').getContext('2d');

    var img = new Image();
    img.src = 'images/papel.jpg';
    img.addEventListener('load', drawTiles, false);

    var boardSize = document.getElementById('puzzle').width;
    var tileCount = 3;

    var tileSize = boardSize / tileCount;

    var clickLoc = new Object();
    clickLoc.x = 0;
    clickLoc.y = 0;

    var emptyLoc = new Object();
    emptyLoc.x = 0;
    emptyLoc.y = 0;

    var solved = false;

    var boardParts;

    setBoard();

    $('#puzzle').click(function (e) {
        clickLoc.x = Math.floor((e.pageX - this.offsetLeft) / tileSize);
        clickLoc.y = Math.floor((e.pageY - this.offsetTop) / tileSize);
        if (distance(clickLoc.x, clickLoc.y, emptyLoc.x, emptyLoc.y) == 1) {
            slideTile(emptyLoc, clickLoc);
            drawTiles();
        }
//        showContactForm(); // solo par apruebas
        if (solved) {
            clearInterval(tiempoCorriendo);
            setTimeout(function () {
                showContactForm();
            }, 500);
        }
    });

    function initBoard() {
        img = new Image();
        img.src = 'images/papel.jpg';
        img.addEventListener('load', drawTiles, false);

        clickLoc = new Object();
        clickLoc.x = 0;
        clickLoc.y = 0;

        emptyLoc = new Object();
        emptyLoc.x = 0;
        emptyLoc.y = 0;

        solved = false;
    }

    function setBoard() {
        initBoard();

        boardParts = new Array(tileCount);
        for (var i = 0; i < tileCount; ++i) {
            boardParts[i] = new Array(tileCount);
            for (var j = 0; j < tileCount; ++j) {
                boardParts[i][j] = new Object;
                boardParts[i][j].x = (tileCount - 1) - i;
                boardParts[i][j].y = (tileCount - 1) - j;
            }
        }
        emptyLoc.x = boardParts[tileCount - 1][tileCount - 1].x;
        emptyLoc.y = boardParts[tileCount - 1][tileCount - 1].y;
        solved = false;
    }

    function drawTiles() {
        context.clearRect(0, 0, boardSize, boardSize);
        for (var i = 0; i < tileCount; ++i) {
            for (var j = 0; j < tileCount; ++j) {
                var x = boardParts[i][j].x;
                var y = boardParts[i][j].y;
                if (i != emptyLoc.x || j != emptyLoc.y || solved == true) {
                    context.drawImage(img, x * tileSize, y * tileSize, tileSize, tileSize,
                            i * tileSize, j * tileSize, tileSize, tileSize);
                }
            }
        }
    }

    function distance(x1, y1, x2, y2) {
        return Math.abs(x1 - x2) + Math.abs(y1 - y2);
    }

    function slideTile(toLoc, fromLoc) {
        if (!solved) {
            boardParts[toLoc.x][toLoc.y].x = boardParts[fromLoc.x][fromLoc.y].x;
            boardParts[toLoc.x][toLoc.y].y = boardParts[fromLoc.x][fromLoc.y].y;
            boardParts[fromLoc.x][fromLoc.y].x = tileCount - 1;
            boardParts[fromLoc.x][fromLoc.y].y = tileCount - 1;
            toLoc.x = fromLoc.x;
            toLoc.y = fromLoc.y;
            checkSolved();
        }
    }

    function checkSolved() {
        var flag = true;
        for (var i = 0; i < tileCount; ++i) {
            for (var j = 0; j < tileCount; ++j) {
                if (boardParts[i][j].x != i || boardParts[i][j].y != j) {
                    flag = false;
                }
            }
        }
        solved = flag;
    }

    /*
     *   VALIDAR CEDULA
     */
    function validarCedula(numeroCedula, errorField) {
        var suma = 0;
        var residuo = 0;
        var pri = false;
        var pub = false;
        var nat = false;
        var modulo = 11;
        var digitoVerificador;

        var p1;
        var p2;
        var p3;
        var p4;
        var p5;
        var p6;
        var p7;
        var p8;
        var p9;

        if (numeroCedula == null || numeroCedula == "") {
            $(errorField).html("Ingresa tu número de cédula"); // CAMPOR VACIO
            return false;
        }
        if (numeroCedula == "0000000000") {
            d$(errorField).html("Tu número de cédula es incorrecto"); // CAMPOR VACIO
            return false;
        }
        if (numeroCedula.length < 10) {
            $(errorField).html("Tu número de cédula es incorrecto"); // MENOS DE 10 CARACTERES
            return false;
        }
        if (isNaN(numeroCedula) == true) {
            $(errorField).html("Tu número de cédula es incorrecto"); // SOLO NUMEROS
            return false;
        }
        // almacenamos los digitos de la cedula en variables.
        var d1 = numeroCedula.slice(0, 1);
        var d2 = numeroCedula.slice(1, 2);
        var d3 = numeroCedula.slice(2, 3);
        var d4 = numeroCedula.slice(3, 4);
        var d5 = numeroCedula.slice(4, 5);
        var d6 = numeroCedula.slice(5, 6);
        var d7 = numeroCedula.slice(6, 7);
        var d8 = numeroCedula.slice(7, 8);
        var d9 = numeroCedula.slice(8, 9);
        var d10 = numeroCedula.slice(9, 10);
        /* El tercer digito es: */
        /* 9 para sociedades privadas y extranjeros */
        /* 6 para sociedades publicas */
        /* menor que 6 (0,1,2,3,4,5) para personas naturales */
        if (d3 == 7 || d3 == 8) {
            $(errorField).html("Tu número de cédula es incorrecto"); // El tercer dígito ingresado es inválido;
            return false;
        }
        /* Solo para personas naturales (modulo 10) */
        if (d3 < 6) {
            nat = true;
            p1 = d1 * 2;
            if (p1 >= 10)
                p1 -= 9;
            p2 = d2 * 1;
            if (p2 >= 10)
                p2 -= 9;
            p3 = d3 * 2;
            if (p3 >= 10)
                p3 -= 9;
            p4 = d4 * 1;
            if (p4 >= 10)
                p4 -= 9;
            p5 = d5 * 2;
            if (p5 >= 10)
                p5 -= 9;
            p6 = d6 * 1;
            if (p6 >= 10)
                p6 -= 9;
            p7 = d7 * 2;
            if (p7 >= 10)
                p7 -= 9;
            p8 = d8 * 1;
            if (p8 >= 10)
                p8 -= 9;
            p9 = d9 * 2;
            if (p9 >= 10)
                p9 -= 9;
            modulo = 10;
        } else if (d3 == 6) {
            // Solo para sociedades publicas (modulo 11) */
            /* Aqui el digito verficador esta en la posicion 9, en las otras 2 en la pos. 10 */
            pub = true;
            p1 = d1 * 3;
            p2 = d2 * 2;
            p3 = d3 * 7;
            p4 = d4 * 6;
            p5 = d5 * 5;
            p6 = d6 * 4;
            p7 = d7 * 3;
            p8 = d8 * 2;
            p9 = 0;
        } else if (d3 == 9) {
            /* Solo para entidades privadas (modulo 11) */
            pri = true;
            p1 = d1 * 4;
            p2 = d2 * 3;
            p3 = d3 * 2;
            p4 = d4 * 7;
            p5 = d5 * 6;
            p6 = d6 * 5;
            p7 = d7 * 4;
            p8 = d8 * 3;
            p9 = d9 * 2;
        }
        //alert(">>>>>> : " +  nat  + " - " + pub + " - " + pri);
        suma = p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;
        residuo = suma % modulo;
        /* Si residuo=0, dig.ver.=0, caso contrario 10 - residuo*/
        if (residuo == 0) {
            digitoVerificador = 0;
        } else {
            digitoVerificador = modulo - residuo;
        }
        //digitoVerificador =  ? 0: modulo - residuo;
        /* ahora comparamos el elemento de la posicion 10 con el dig. ver.*/
        if (pub == true) {
            if (digitoVerificador != d9) {
                $(errorField).html("Tu número de cédula es incorrecto"); // El ruc de la empresa del sector público es incorrecto.;
                return false;
            }
            /* El ruc de las empresas del sector publico terminan con 0001*/
            if (numeroCedula.slice(9, 4) != '0001') {
                $(errorField).html("Tu número de cédula es incorrecto"); // El ruc de la empresa del sector público debe terminar con 0001";
                return false;
            } else if (pri == true) {
                if (digitoVerificador != d10) {
                    $(errorField).html("Tu número de cédula es incorrecto"); //"El ruc de la empresa del sector privado es incorrecto.";
                    return false;
                }
                if (numeroCedula.slice(10, 3) != '001') {
                    $(errorField).html("Tu número de cédula es incorrecto"); //"El ruc de la empresa del sector privado debe terminar con 001";
                    return false;
                }
            }
        } else if (nat == true) {
            if (digitoVerificador != d10) {
                $(errorField).html("Tu número de cédula es incorrecto"); // "El número de cédula de la persona natural es incorrecto.";
                return false;
            }
            if (numeroCedula.length > 10 && numeroCedula.slice(10, 3) != '001') {
                $(errorField).html("Tu número de cédula es incorrecto"); // "El ruc de la persona natural debe terminar con 001";
                return false;
            }
        }
        return true;
    }
});